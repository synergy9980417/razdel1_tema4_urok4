import nasledovanie2.AbstractNasledovanie2;
import nasledovanie2.MyAdmin;
import nasledovanie2.MyModer;
import nasledovanie2.MyUser;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.


//        Урок 4. Преобразование наследников к родителю.
//        1.
//        Почему наследник всегда можно преобразовать к родителю?
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("Видимо потомучто наследник обладает всеми полями и методами родителя");
//                2.
//        Реализуйте метод, который выводит имя пользователя. Передайте в этот
//        метод также и модератора, и админа
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!");

        MyUser user = new MyUser("Фёдор");
        getThisName(user);
        getThisName(new MyModer("Павло"));

        getThisName(new MyAdmin("Сурик"));


//        3.
//       Возможно ли преобразовать родителя к наследнику? Попробуйте

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!!!!!!!");

//        getThisParent((MyUser) new AbstractNasledovanie2("Акакий"));
        System.out.println("выбрасывается исключение в этом случае. поэтому комментируем 36 строку");
//только через приведение типов

//        4.
//        Реализуйте метод, который возвращает родителя, а не наследника. Какой в
//        этом смысл?

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
        AbstractNasledovanie2 tes = parentFromChield(new MyUser("Паша"));
        System.out.println(
                tes
        );
        // практического применения в этом не вижу


//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написана общая структура программы
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно  более 80% технического задания, а также
//        продемонстрированы теоретические знания на удовлетворительном уровне
//        5 баллов
//                -
//                все технические задания выполнены ко
//        рректно, в полном объеме,
//        продемонстрированы  отличные теоретические знания при ответе на вопросы
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов

    }

    public static void getThisName(AbstractNasledovanie2 user) {
        System.out.println(user.name);
    }
    public static void getThisParent(MyUser user) {
        System.out.println(user.name);
    }

    public static AbstractNasledovanie2 parentFromChield(MyUser user){
        System.out.println("возвращаем родителя по данным наследника");
        return new AbstractNasledovanie2(user.name);
    }


}